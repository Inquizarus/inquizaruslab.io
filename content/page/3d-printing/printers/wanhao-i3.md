---
title: "Wanhao Duplicator i3"
---
The first of all my machines, legendary bringer of madness and knowledge about this hobby.

Wanhao i3 printers comes in a few different versions, this one was an original v1 with all its flaws but have over the years been converted into something more similar to their
current [i3 v2.1](https://www.wanhao3dprinter.com/Unboxin/ShowArticle.asp?ArticleID=70 "Wanhao Duplicator i3 v2.1 page").

## Firmware
After upgrading from the stock Melzi board during 2020 to a 32bit board from BTT the printer is running version 2.0.7.2 [Marlin firmware](https://marlinfw.org "Marlin Firmware Website") rather than what comes from the factory.

## Hardware specifications

**Board:** [BTT SKR 1.4](https://www.bigtree-tech.com/products/btt-skr-v1-4-skr-v1-4-turbo-32-bit-control-board.html).

**Display:** [BTT TFT35 v3.0](https://www.biqu.equipment/products/bigtreetech-tft35-v3-0-display-two-working-modes).

**Hotend:** [All metal from Micro Swiss](https://store.micro-swiss.com/collections/all-metal-hotend-kits/products/all-metal-hotend-with-slotted-cooling-block-for-wanhao-i3).

**Extruder lever & plate:** [Machined Lever and Extruder from Micro Swiss](https://store.micro-swiss.com/collections/extruders/products/lever-for-wanhao-i3-extruder).

**Y-Carriage:** [Four bearing carriage from Tehnologika](https://tehnologika.net/Wanhao-duplicator-i3-composit-heated-bed-support-y-carriage-plate-reprap/).

**Bed springs:** [Creality yellow springs](https://www.3dprima.com/se/parts/spare-parts/creality-3d/creality-3d-leveling-spring/a-23884).

**Probe:** [BLTouch v3.1](https://www.antclabs.com/bltouch-v3).

**Bed:** Cut to size glass for flatness and [PrimaCreator PEI FlexPlate](https://primacreator.com/products/primacreator-flexplate-pei) ontop of it.

**Bearings:** [Brackets and bearings](https://www.3dprima.com/se/parts/spare-parts/wanhao-i3/wanhao-bearing-with-bracket-di3-plus-di3-2-1/a-22012) from Di3 v2.1 on both X and Y axis.

**Belts:** Stock with springs removed.

## Cabling
Most cabling is stock except for the extruder motor which was re-crimped to JST XH with the new board, z-endstop is not used since BLTouch acts as a more accurate one.
Largest change have been replacing most cabling going to the gantry/extruder block with a Cat5 cable, only heating, stepper and BLTouch remain separate from the Cat5 cable.

## Printed upgrades
* [Z-Braces](https://www.thingiverse.com/thing:921948)
* [EiiCooler](https://www.thingiverse.com/thing:2060590)
* [OptiCooler](https://www.thingiverse.com/thing:2927210)
* [Numbered Thumbwheels](https://www.thingiverse.com/thing:874155)
* [Ultimate X-belt tensioner](https://www.thingiverse.com/thing:1820493)
* [Super simple Y-belt tensioner](https://www.thingiverse.com/thing:1784375)
