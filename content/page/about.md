---
title: About me
subtitle: A swedish developer
comments: false
---
I have been working as a developer in some shape and/or form since 2013, middling with all layers of
applications in addition to having an interest in the infrastructure that they run on and what it all brings
as business/customer value.

Nowadays my work focuses on Cloud infrastructure and spreading the DevOps love around the workplace.  
