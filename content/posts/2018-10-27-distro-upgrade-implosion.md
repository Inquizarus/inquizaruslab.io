---
title: "When your distro upgrade backfires"
date: 2018-10-27
tags: [linux, doom, thisisfine]
---
So.. The bad thing happened, I went on to upgrade my ElementaryOS version to the new shiny big 5.0 release and kind of screwed it up. But that's okey I guess?

![](https://i.giphy.com/media/z9AUvhAEiXOqA/giphy.webp)



## Why did this happen?

Primarily I just tend to split root partition from my home one, have been my goto scheme for years when it comes to running Linux. Gives good flexibility and allows me to upgrade however I want.

But then... There is this thing called home directory encryption, which was removed in Ubuntu 18.04 that ElementaryOS 5.0 is based on.

I formatted my `/` partition, mounted my `/home` without formatting and went on. Created a user with the same name as my old one. When installer was complete and laptop had rebooted I tried logging in and where promptly tossed back to the login screen. Oh no..

I so wanted this to be a smooth experience, without rolling back to a previous release and things like that. But having forgotten that I encrypted my home directory it seems like I have locked myself out.



## And why it's ok

There is one thing that I have picked up as a habit, whenever I reinstall my machine I backup my SSH keys on the same USB that the new installer is on.  So I thought about which losses I would have by just rolling with this situation.

- configurations for software
- development folder with all my code
  - My main pain point, but actually a lesser problem than one might think since I have remote
    repositories for all of it. Connected with my ssh keys.
- Nothing else?

In reality, even if I had lost my SSH keys it would be ok. But a much larger hassle!

Meanwhile I would win a clean slate, a chance to automate my initial machine set up. I had started a dot-files repository a few years back.



### Executing plan B

I quickly redid the installation, now creating a new account while keeping my old one. If I find that the new approach falls short or something I want to have a possibility to roll back.

Laptop rebooted, I logged in and moved over the SSH keys which of course needed a little chmod+chown action.

Loaded up the ssh key with a`ssh-add -k` and cloned my dot-files repository from Github. It severely lacked some things but after tweaking it I'm up and running again. Writing this post and everything. With a nice and fresh installation to boot. Which will make it even easier to get up and running on any future computer.

### What did I learn?

* Keep those SSH keys protected when doing new installations
* Keep as much important things as possible under version control
  or even on some type of cloud storage
* Automate the initial set up of as much as possible. The only thing I really did manually where
  installing git.
