---
title: "Up and running with FoundryVTT part 1 - The container image"
sub: "asdasd"
date: 2020-04-11
tags: [linux, role-playing, containerization]
aliases: ["/posts/foundryvtt-part-1"]
---
One of the problems that groups of role-players might run into is that they can’t get together because of varying reasons like geographical, COVID-19 or whatever else.

This can be solved with technologies such as Skype, Discord, Google Hangouts or Zoom. The base premise is that everyone should be able to communicate and preferably see each other and the rest could be done in the "theatre of the mind" way.

VTT software solves this and usually add many other functions that would make it more akin to actually getting together to run a session.

---

_During a few articles I will describe how I get **FoundryVTT** up and running containerized, in the cloud and behind a reverse-proxy that handles
SSL termination to beat._

After this article you should have a containerized FoundryVTT server running locally which can be accessed through `http://localhost:30000`.

## What is VTT

What it says on the tin, a virtual tabletop.

To elaborate on this, most VTT software comes with one or more of the following features

* Battle map with grid snapping functionality to visually show what is currently happening
* Dice rolling tools
* Chat for all participants
* RPG system integration to allow easy references to game rules, including character sheets
* Video and/or Audio functionality to communicate with each other
* Calculation automation

And much more, but even with that short list one can see that it will give quite a different experience running a session online.

Another thing to note is cost, most if not all VTT software comes with a cost in one way or another. Either as a subscription or as
a one-time fee, depending on which distribution models it uses.

In most cases if the VTT is used as a SaaS(Software as a Service) they will follow a **freemium** model where at least one (usually the Game Master) will have to
have a monthly subscription to unlock "vital" functionality for the rest of the group. In general the subscription cost would range from $5 to $10 per month.

Other VTT software are instead sold as a one time licence cost where you yourself will run a server that everyone connects to, this is usually a larger cost initially at around
$40 to $60.

There are benefits to both models for consumers, just as with SaaS platforms overall tech you gain "ease of use" while installing and hosting things yourself get you flexibility. Use your own (or the groups) judgement to make a wise decision.

## What is Foundry VTT

So lets narrow things down to the VTT of choice for me, namely [FoundryVTT](https://foundryvtt.com/).

This is a new up-and-coming VTT software where you purchase a licence and can host your own server, they have partners which
enables you to avoid having to set up everything yourself.

Its so new that it haven't even officially released yet. The release date is currently May 22 with a pre-purchase available from April 17. One can get access to the current
beta version by subscribing to the developers Patreon page.

The main reason I choose this software over other alternatives is that it aims to be very open and extendable and is built with modern technology that enables the community
to build their own content to use on the platform.

I had never used a VTT before since I value the social aspect greatly when it comes to role-playing, but the [feature set](https://foundryvtt.com/article/overview/) was very convincing. Knowing that I wanted to host a server myself and have the possibility to stick it into the cloud made FoundryVTT more or less the only option remaining to beat.

## Let's get started

### Assumptions

* Having basic knowledge of using containers
* Having basic knowledge of the command line
* Working in a UNIX OS such as Ubuntu

### Obtain the software

First you have to purchase and download the software. No news there.

Put the downloaded zip (lets call it foundryvtt.zip) in the directory where you want to work on this.

### The container definition

The container definition for this is quite simple really, FoundryVTT is served by a NodeJS server which requires the port 30000. So we can use a NodeJS
alpine image as a base and work from there.

```docker
FROM node:12-alpine

RUN mkdir /data
COPY app /app
COPY entrypoint.sh .

EXPOSE 30000
ENTRYPOINT "/entrypoint.sh"
```

We create a `/data` directory at the root of our container which will hold persistent data about our game worlds, then copy a local folder called `app` to `/app` which will hold all application data related to the FoundryVTT server.

Then we expose the needed port of `30000` and lastly point to an `entrypoint.sh` file which will allow us to be a bit more flexible with the upstart of our
FoundryVTT server if we want/need to.

This is all saved as a `Dockerfile` which we then can use for building our image later.

### The entrypoint

In the container definition we declared that the entrypoint would be `/entrypoint.sh`, so let us create that file with the following content.

```shell
#!/bin/sh

node /app/resources/app/main.js --headless --dataPath=/data
```

This is not anything that the Dockerfile could not handle but having it as a script we have the flexibility to do more things if we need/want to just as I mentioned earlier.

We run the file `main.js` with node and pass `--headless` since we only want the server and no GUI (FoundryVTT comes with an Electron app baked in!) and then the `--dataPath` which just points the FoundryVTT server to where we decided to put all our custom data later.

### Building the image

First things first, as you could see in the definition, we copy an `app` folder into the image. We populate this by running the following commands.

```bash
mkdir app
cd app/
unzip foundryvtt.zip
cd ../
```

After this we can move on to building the image.

```bash
docker build -t inquizarus/foundryvtt:latest .
```

*The name/tag `inquizarus/foundryvtt:latest` does not really matter and you can use whatever you want.*

**NOTE! This image should NEVER be published to a image repository like Docker hub, Quay, GitLab or the like since it contains licensed application data!**

### Running the container

Now, we need to create the `data/` directory first.

```bash
mkdir data
```

Then we should be prepared to run our container based on the image we just built by running the following command.

```bash
docker run --name foundryvtt --rm -d -v "$(pwd)/data:/data" -p "30000:30000" inquizarus/foundryvtt:latest
```

We give it the name **foundryvtt** with `--name` for ease of identification, mark it as being removed when container is stopped with `--rm`, put it in the background using `-d`, attach the `data/` directory as a volume with `-v` for persisting our data and lastly map the port `30000` between the host and container with the `-p` flag.

*`$(pwd)` is used above in the `-v` part to easily get the absolute path to the local data directory since container volumes require that.*

Now you should be able to (in a browser) navigate to `http://localhost:30000` and follow any instructions needed to start using FoundryVTT!

I have created a repository containing all of what have been created in this article [here](https://github.com/Inquizarus/foundryvtt-ctnr) which also contains
some scripts for quality of life things.

### Related articles

* Up and running with FoundryVTT part 1 - The container image (**You're reading!**)
* [Up and running with FoundryVTT part 2 - Moving to the cloud](/posts/foundryvtt-part-2)
* Up and running with FoundryVTT part 3 - Hiding behind a proxy
* Up and running with FoundryVTT part 4 - SSL is a good thing to have

*Articles that aren't linked above are yet to be published.*
