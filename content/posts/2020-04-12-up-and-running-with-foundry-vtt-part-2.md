---
title: "Up and running with FoundryVTT part 2 - Moving to the cloud"
date: 2020-04-12
tags: [linux, role-playing, containerization]
aliases: ["/posts/foundryvtt-part-2"]
---
This is part 2 in a series of articles which describes how you can get a [FoundryVTT](https://foundryvtt.com) server up an running in the cloud.

Let's get going from where we left off in the [previous](/posts/foundryvtt-part-1) article.

After this article you should have a container running on a VPS at some cloud provider. In my case its Digitalocean which have the cheapest VPS machines in the game currently.

## Let's get started

### Assumptions

*In addition to the previous articles assumptions.*

* Having a VPS that is running on a debian based distribution (Ubuntu 19.10 in my case)
* Having ssh access to the server
* Docker is installed on the server and having permissions to use it

### Exporting the image

One of the first things we must do because of obvious reasons are getting the image we created in the previous article
to our server.

As I mentioned in that article, the image containing FoundryVTT application data should NEVER be published. Which leaves out the main
way people (including me) tend to distribute the image to install on our servers.

So how do we go about doing this then?

We must export the image locally!

Docker have a command for exporting an image into a **TAR** file, which one could say is an archive of all image contents+meta information about it that can be moved around just like any other file. Neat!

So run the following command: 

```bash
# replace the image name with whatever name it was given earlier
docker save -o foundryvtt.tar inquizarus/foundryvtt:latest
```

Then to check that it's actually there:

```bash
$ ls -ltr
-rwxr-xr-x 1 johndoe johndoe       184 Apr 11 12:22 entrypoint.sh
-rw-r--r-- 1 johndoe johndoe       115 Apr 11 12:22 Dockerfile
drwxr-xr-x 5 johndoe johndoe      4096 Apr 11 13:34 app
drwxr-xr-x 5 johndoe johndoe      4096 Apr 11 13:34 data
-rw-r--r-- 1 johndoe johndoe 398804480 Apr 13 15:52 foundryvtt.tar # here it is!
```

Great, now that we have it as a "physical" file we can upload it to our server!

### Uploading the image

We can upload it to our server with `scp` like this:

```bash
# replace username, hostname and path with correct values
$ scp foundryvtt.tar <username>@<hostname>:<path to target folder>
...
foundryvtt.tar             44%  171MB  24.8MB/s   00:08 ETA
...
```

When that is done uploading, we can hop unto our server and continue the process.

### Importing the image

After getting on the server and navigating to the path we used in our `scp` command we can verify that the TAR archive have arrived
safely.

```bash
$ ls -ltr
...
-rw-r--r-- 1 johndoe johndoe 398804480 Apr 13 13:58 foundryvtt.tar # here it is!
...
```

We are almost there now!

Import the TAR archive with this command:

```bash
$ docker load < foundryvtt.tar
beee9f30bc1f: Loading layer [==================================================>]  5.862MB/5.862MB
b745c9620c42: Loading layer [==================================================>]   79.4MB/79.4MB
33c549f45c7f: Loading layer [==================================================>]  7.658MB/7.658MB
b12c59827877: Loading layer [==================================================>]  3.584kB/3.584kB
c4332dde59e7: Loading layer [==================================================>]  3.584kB/3.584kB
343527b1bbda: Loading layer [==================================================>]  305.8MB/305.8MB
ac6ac70ce445: Loading layer [==================================================>]  2.048kB/2.048kB
Loaded image: inquizarus/foundryvtt:latest
```

Then verify that it was imported successfully:

```bash
$ docker images
REPOSITORY                        TAG                 IMAGE ID            CREATED             SIZE
inquizarus/foundryvtt             latest              a49f9567ce41        1 days ago          387MB
```

Seems like it!

### Starting it up

Now that the image have been imported on the server, a container can now be started with it exactly the same way as on the local machine.

*Remember to create a `data/` directory on the server before starting the container! (Or upload an existing one from the local machine to migrate any changes done there by using `scp` with the `-r` flag for recursive upload)*

```bash
mkdir data
docker run --name foundryvtt --rm -d -v "$(pwd)/data:/data" -p "30000:30000" inquizarus/foundryvtt:latest
```

Now you should be able to (in a browser) navigate to `http://<server hostname/ip>:30000` and follow any instructions needed to start using FoundryVTT on  the server in exactly the same manner it was done locally!

---

*In the next article I will get more into detail regarding server configuration to for example avoid having to use `:30000` when connecting to FoundryVTT by using a proxy which will later also be used to handle SSL certificates to ensure secure communication between clients and the server. Perhaps there will even be some Terraform examples to set up DNS records amongst other things!* 

### Related articles

* [Up and running with FoundryVTT part 1 - The container image](/posts/foundryvtt-part-1)
* Up and running with FoundryVTT part 2 - Moving to the cloud (**You're reading!**)
* Up and running with FoundryVTT part 3 - Hiding behind a proxy
* Up and running with FoundryVTT part 4 - SSL is a good thing to have

*Articles that aren't linked above are yet to be published.*
