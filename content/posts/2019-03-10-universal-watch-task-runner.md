---
title: "Universal watch task runner"
date: 2019-03-10
tags: [linux, techtips]
---
_This is a quick tip for not having to install a watcher that run tasks like build and test for every different project you have._

The key to this is the **[Entr](http://eradman.com/entrproject)** package. It's purpose is to watch for file changes. Just like you would use `npm run watch` or whatever floats your goat.

First and foremost, install entr.

```bash
sudo apt install entr -y
```

Then, these things are best explained by a practical example.

```bash
ls **/*.go | entr -d sh -c 'go test -cover -v ./...'
```

What the above does:

1. List all go files in current directory level or lower and pass output to next command
2. Feed input from `ls` into `entr`
    * `-d` to check for new files
    * `sh -c` to run a shell command
    * `'go test -cover -v ./...'` is just the command to run when something changes

For frontend projects I use something similar to this

```bash
ls frontend/**/*.* | entr -d npm run dev
```

_where `npm run dev` will run tests and build things._

**Entr** is very flexible, I have used it to do the classic TDD workflow, having integration tests running in the background with curl and much more. Have fun with it!